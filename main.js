'use strict';
let slider = document.querySelector(".slider_block");
let images = document.querySelectorAll(".slide");

let nextBtn = document.querySelector(".next");
let prevBtn = document.querySelector(".prev");

let counter = 1;
let width = images[0].clientWidth;
slider.style.transform = 'translateX(' + (-width * counter) + 'px)';

nextBtn.addEventListener("click", function(){
  if(counter >= images.length-1) return;
  slider.style.transition = "transform .5s ease-in-out";
  counter++;
  slider.style.transform = 'translateX(' + (-width * counter) + 'px)';
});


prevBtn.addEventListener("click", function(){
  if(counter <=0) return;
  slider.style.transition = "transform .5s ease-in-out";
  counter--;
  slider.style.transform = 'translateX(' + (-width * counter) + 'px)';
});

slider.addEventListener("transitionend", ()=>{
  console.log(images[counter]);
let last = document.getElementById("last");
let first = document.getElementById("first");
  if(images[counter] === last){
    slider.style.transition = "none";
    counter = images.length - 2;
    slider.style.transform = 'translateX(' + (-width * counter) + 'px)';
  }

  if(images[counter] === first){
    slider.style.transition = "none";
    counter = images.length - counter;
    slider.style.transform = 'translateX(' + (-width * counter) + 'px)';
  }
})







